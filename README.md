[1]: https://bitbucket.org/rteyssie/ramses/
[2]: https://bitbucket.org/ableuler/mini-ramses/wiki

## mini-ramses ##

The mini-ramses repository is a fork of the [main RAMSES repository][1]. It was created as a stripped-down version of the main code base created in order to facilitate the development of major updates of RAMSES' core routines. This stripped-down version is still available in the `master` branch.

A rapidly-evolving development of mini-ramses based on a completely changed data-structure is contained in the `develop` branch. This branch is probably buggy, use it only if you know what you're doing ;)


You can download the code by cloning the git repository using 
```
$ git clone https://bitbucket.org/ableuler/mini-ramses.git
```

If you want to contribute to mini-ramses, you can either ask me (ableuler@physik.uzh.ch) for your personal new branch in this repository which I will give you write access to, or you can fork this repository. To bring changes back into the `develop` branch of mini-ramses, simply issue a pull request.